import unittest
from main import modif_delimiteur
from main import modif_nom
from main import reorganisation 

class TestHistoireAutomobile(unittest.TestCase):

    def setUp(self):
        print("Nous sommes avant le test")

    def test_modif_nom(self):
        self.assertEqual("prenom", modif_nom("firstname"))
        self.assertEqual("energie", modif_nom("energy"))
        self.assertEqual("nom", modif_nom("name"))
    
    def test_modif_delimiteur(self):
        self.assertEqual("spam;lovely", modif_delimiteur("spam|lovely",';'))
        self.assertEqual("spam/wonderful", modif_delimiteur("spam|wonderful",'/'))

    def test_reorganisation(self) : 
        before_organisation = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,"ar_bc_cd",17]
        After_organisation = reorganisation(before_organisation)
        self.assertEqual(1 , After_organisation[0])
        self.assertEqual(17 , After_organisation[5])
        self.assertEqual(2 , After_organisation[9])
        self.assertEqual('ar', After_organisation[16])
        self.assertEqual('bc', After_organisation[17])
        self.assertEqual('cd', After_organisation[18])

    def tearDown(self) : 
        print("Nous sommes après le test")