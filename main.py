import csv
import argparse 

#permet la modification des délimiteurs dans une chaine de charactère
def modif_delimiteur(chaineSaisie, nouveauDelimiteur):
    return chaineSaisie.replace('|',nouveauDelimiteur)

#permet de changer le format de la date : de j/m/y à y/m/j et vice versa
def change_date_format(dateAModifier):
    if dateAModifier != "date_immat":
        val=dateAModifier.split('-')
        year = val[0] 
        month = val[1]
        day = val[2]
        dateAModifier = day + '-' + month + '-' + year 
    return dateAModifier

#permet d'initialiser les données pour echanger le nom des colonnes 
def init_translation():
    traduction = {
        'address': {'trad': 'adresse_titulaire'},
        'carrosserie': {'trad': 'carrosserie'},
        'categorie': {'trad': 'categorie'},
        'couleur': {'trad': 'couleur'},
        'cylindree': {'trad': 'cylindree'},
        'date_immat': {'trad': 'date_immatriculation'},
        'denomination': {'trad': 'denomination_commerciale'},
        'energy': {'trad': 'energie'},
        'firstname': {'trad': 'prenom'},
        'immat': {'trad': 'immatriculation'},
        'marque': {'trad': 'marque'},
        'name': {'trad': 'nom'},
        'places': {'trad': 'places'},
        'poids': {'trad': 'poids'},
        'puissance': {'trad': 'puissance'},
        'type': {'trad': 'type'},
        'variante': {'trad': 'variante'},
        'version': {'trad': 'version'},
        'vin': {'trad': 'vin'},
    }
    return traduction

#permet de modifier le nom des colonnes
def modif_nom(chaineSaisie):
    traduction = init_translation()
    for key in traduction:
        if key == chaineSaisie:
            for result in traduction[key].values():
                return result
    return "nom fourni incorrect"

#permet de réorganisé les données du tableau
def reorganisation(tableauAModifier):
    val=tableauAModifier[15].split('_')
    nouvelOrdre = [0,11,8,9,5,16,10,6,3,1,2,4,7,12,13,14]
    tableauAModifier = [tableauAModifier[value] for value in nouvelOrdre]
    tableauAModifier.extend(res for res in val) 
    tableauAModifier[4] = change_date_format(tableauAModifier[4])
    return tableauAModifier

#permet d'enregistrer les données fournis tout en modifiant le nom des collones 
def recup_ligne_header(chaineSuivante, data, firstdata):
    if firstdata:
        chaineSuivante += modif_nom(data)
    else:
        chaineSuivante += "|" + modif_nom(data)
    return chaineSuivante, False

#permet d'enregistrer les données fournis 
def recup_ligne(chaineSuivante, data, firstdata):
    if firstdata:
        chaineSuivante += data
    else:
        chaineSuivante += "|" + data
    return chaineSuivante, False

#permet de recuperer les données d'une seul ligne
def action_sur_ligne(touteChaine, row, ligneUn):
    firstdata = True
    for data in row:
        if ligneUn: 
            touteChaine, firstdata = recup_ligne_header(touteChaine, data, firstdata)
        else:
            touteChaine, firstdata = recup_ligne(touteChaine, data, firstdata) 
    touteChaine += '\n'
    return touteChaine, False

#permet de récuperer toutes les données du fichier, ligne par ligne
def recup_toute_ligne(reader):
    touteChaine =""
    ligneUn = True
    for row in reader:
        nouvelleOrganisation = reorganisation(row)
        touteChaine, ligneUn = action_sur_ligne(touteChaine, nouvelleOrganisation, ligneUn)
    return touteChaine

#Code principal
#Code permettant la mise en place des informations par rapport aux argument a fournir
parser=argparse.ArgumentParser(description="This a usefull description")
parser.add_argument("cheminFichierACopier", help="Ecrire le chemin du fichier a modifier", type=str)
parser.add_argument("delimiteurNouveauFichier", help="Ecrire le nouveau delimiteur du fichier .csv", default=';')
args = parser.parse_args()

#permet la copie et la modification des données d'un fichier fournis avec sont delimiteur
with open(args.cheminFichierACopier) as csvfile:
    reader = csv.reader(csvfile, delimiter=';') 
    listChaine = recup_toute_ligne(reader)
listChaine = modif_delimiteur(listChaine, args.delimiteurNouveauFichier)

#permet l'ecriture dans un nouveau fichier
with open('test2.csv', 'w') as csvfilewrite:
    csvfilewrite.write(listChaine)